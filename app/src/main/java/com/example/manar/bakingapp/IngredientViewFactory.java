package com.example.manar.bakingapp;

import android.content.Context;
import android.os.Bundle;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import com.example.manar.bakingapp.model.Ingredient;
import com.example.manar.bakingapp.model.RecipeSteps;

/**
 * Created by manar on 15/12/17.
 */

public class IngredientViewFactory implements RemoteViewsService.RemoteViewsFactory {

    private RecipeSteps recipeSteps ;
    private Context context ;

    public IngredientViewFactory(Context context) {
        this.context = context;
    }

    @Override
    public void onCreate() {
        recipeSteps = SharedPrefrence.getData(context);
    }

    @Override
    public void onDataSetChanged() {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public int getCount() {
        return recipeSteps.getIngredients().size();
    }

    @Override
    public RemoteViews getViewAt(int i) {
        final RemoteViews remoteView = new RemoteViews(
                context.getPackageName(), R.layout.ingredients_item);
        Ingredient listItem = recipeSteps.getIngredients().get(i);
        remoteView.setTextViewText(R.id.quantity, String.valueOf(listItem.getQuantity()));
        remoteView.setTextViewText(R.id.measure, listItem.getMeasure());
        remoteView.setTextViewText(R.id.ingredients, listItem.getIngredient());
        Bundle extras = new Bundle();
        extras.putInt(IngredientsWidgetService.EXTRA_ITEM, i);
        return remoteView;
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return 1 ;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }
}
