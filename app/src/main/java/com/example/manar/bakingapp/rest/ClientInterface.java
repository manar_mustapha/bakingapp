package com.example.manar.bakingapp.rest;

import com.example.manar.bakingapp.model.RecipeSteps;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by manar on 01/12/17.
 */

public interface ClientInterface {

    @GET("topher/2017/May/59121517_baking/baking.json")
    Call<List<RecipeSteps>> getRecipeList ();

}
