package com.example.manar.bakingapp.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.manar.bakingapp.R;
import com.example.manar.bakingapp.SharedPrefrence;
import com.example.manar.bakingapp.adapter.RecipeAdapter;
import com.example.manar.bakingapp.contoller.GetRecipeController;
import com.example.manar.bakingapp.model.RecipeSteps;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecipiesActivity extends AppCompatActivity {

    RecyclerView recipeRecycleView;
    RecipeAdapter recipeAdapter;
    ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        progressBar = findViewById(R.id.progress);
        recipeRecycleView = findViewById(R.id.recipe_list);
        recipeRecycleView.hasFixedSize();
        recipeRecycleView.setLayoutManager(new GridLayoutManager(this,numberOfColumns()));
    }

    private int numberOfColumns() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        // You can change this divider to adjust the size of the poster
        int widthDivider = 600;
        int width = displayMetrics.widthPixels;
        int nColumns = width / widthDivider;
        if (nColumns < 2) return 1;
        return nColumns;
    }
    @Override
    protected void onStart() {
        super.onStart();
        progressBar.setVisibility(View.VISIBLE);
        GetRecipeController getRecipeController = new GetRecipeController();
        getRecipeController.getRecipe(new Callback<List<RecipeSteps>>() {
            @Override
            public void onResponse(Call<List<RecipeSteps>> call, Response<List<RecipeSteps>> response) {
                if(response.body() != null){
                    progressBar.setVisibility(View.INVISIBLE);
                    SharedPrefrence.saveData(response.body().get(0),RecipiesActivity.this);
                    recipeAdapter = new RecipeAdapter(response.body(), getBaseContext(), new RecipeAdapter.OnClickHandler() {
                        @Override
                        public void onRecipeClicked(RecipeSteps recipeSteps) {
                            SharedPrefrence.saveData(recipeSteps,RecipiesActivity.this);
                            Log.v("recipeStep",recipeSteps.getName());
                            Intent intent = new Intent(RecipiesActivity.this , RecipeDetailsActivity.class);
                            intent.putExtra("RecipeDetailsActivity",recipeSteps);
                            startActivity(intent);
                        }
                    });
                    recipeRecycleView.setAdapter(recipeAdapter);
                }else{
                    progressBar.setVisibility(View.INVISIBLE);
                    Toast.makeText(RecipiesActivity.this,"Error", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<List<RecipeSteps>> call, Throwable t) {
                progressBar.setVisibility(View.INVISIBLE);
                Toast.makeText(RecipiesActivity.this,t.getMessage() , Toast.LENGTH_LONG).show();
            }
        });
    }
}