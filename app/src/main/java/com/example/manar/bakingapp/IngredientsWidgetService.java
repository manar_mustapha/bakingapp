package com.example.manar.bakingapp;

import android.content.Intent;
import android.widget.RemoteViewsService;

/**
 * Created by manar on 15/12/17.
 */

public class IngredientsWidgetService extends RemoteViewsService {

    public static final String ACTION_UPDATE_WIDGETS = "android.appwidget.action.APPWIDGET_UPDATE";
    public static final String EXTRA_ITEM = "com.example.edockh.EXTRA_ITEM";

    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        return new IngredientViewFactory(getApplicationContext());
    }
}
