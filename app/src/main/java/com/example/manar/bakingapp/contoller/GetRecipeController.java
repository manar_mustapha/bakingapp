package com.example.manar.bakingapp.contoller;

import com.example.manar.bakingapp.model.RecipeSteps;
import com.example.manar.bakingapp.rest.ApiClient;
import com.example.manar.bakingapp.rest.ClientInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by manar on 01/12/17.
 */

public class GetRecipeController {

    public void getRecipe(Callback<List<RecipeSteps>> callback) {
        ClientInterface apiServiceGetRecipe = ApiClient.getClient().create(ClientInterface.class);
        Call<List<RecipeSteps>> rateOrder = apiServiceGetRecipe.getRecipeList();
        rateOrder.enqueue(callback);
    }
}
