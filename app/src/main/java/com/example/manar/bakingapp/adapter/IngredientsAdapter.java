package com.example.manar.bakingapp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.manar.bakingapp.R;
import com.example.manar.bakingapp.model.Ingredient;

import java.util.List;

/**
 * Created by manar on 02/12/17.
 */

public class IngredientsAdapter extends RecyclerView.Adapter<IngredientsAdapter.IngredientsViewHolder> {

    List<Ingredient> ingredientList;

    public IngredientsAdapter(List<Ingredient> ingredientList) {
        this.ingredientList = ingredientList;
    }

    @Override
    public IngredientsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ingredients_item, parent, false);
        return new IngredientsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(IngredientsViewHolder holder, int position) {
        holder.setData(ingredientList.get(position));
    }

    @Override
    public int getItemCount() {
        return ingredientList.size();
    }

    class IngredientsViewHolder extends RecyclerView.ViewHolder {

        TextView ingredients;
        TextView quantity;
        TextView measure;

        IngredientsViewHolder(View itemView) {
            super(itemView);
            ingredients = itemView.findViewById(R.id.ingredients);
            quantity = itemView.findViewById(R.id.quantity);
            measure = itemView.findViewById(R.id.measure);
        }

        void setData(Ingredient ingredient) {
            ingredients.setText(ingredient.getIngredient());
            quantity.setText(ingredient.getQuantity()+"" );
            measure.setText(ingredient.getMeasure());
        }
    }
}