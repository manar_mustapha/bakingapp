package com.example.manar.bakingapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.manar.bakingapp.R;
import com.example.manar.bakingapp.model.RecipeSteps;
import com.example.manar.bakingapp.model.Step;

import java.util.List;

/**
 * Created by manar on 01/12/17.
 */

public class DescriptionAdapter extends RecyclerView.Adapter<DescriptionAdapter.DescriptionViewHolder> {

    private DescriptionAdapter.OnClickHandler onClickHandler;
    private List<Step> stepList;
    private Context context;

    public DescriptionAdapter(DescriptionAdapter.OnClickHandler onClickHandler, List<Step> stepList, Context context) {
        this.onClickHandler = onClickHandler;
        this.stepList = stepList;
        this.context = context;
    }

    @Override
    public DescriptionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.description_item, parent, false);
        return new DescriptionViewHolder(view);
    }

    public interface OnClickHandler {
        void onShortDescClicked(Step step);
    }

    @Override
    public void onBindViewHolder(DescriptionViewHolder holder, int position) {
        holder.setShortDesc(stepList.get(position).getShortDescription());
    }

    @Override
    public int getItemCount() {
        return stepList.size();
    }

    class DescriptionViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView shortDescription ;

        DescriptionViewHolder(View itemView) {
            super(itemView);
            shortDescription = itemView.findViewById(R.id.short_desc);
            itemView.setOnClickListener(this);
        }

        void setShortDesc(String shortDesc){
            shortDescription.setText(shortDesc);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            onClickHandler.onShortDescClicked(stepList.get(position));
        }
    }
}