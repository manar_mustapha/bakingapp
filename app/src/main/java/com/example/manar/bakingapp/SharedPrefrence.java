package com.example.manar.bakingapp;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.manar.bakingapp.model.RecipeSteps;
import com.google.gson.Gson;

import static android.R.attr.value;

/**
 * Created by manar on 15/12/17.
 */

public class SharedPrefrence {

    public static void saveData(RecipeSteps recipeSteps,Context context){
        SharedPreferences preferences = context.getSharedPreferences("recipe", Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = preferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(recipeSteps);
        prefsEditor.putString("recipe", json);
        prefsEditor.apply();
    }

    public static RecipeSteps getData(Context context){
        SharedPreferences preferences = context.getSharedPreferences("recipe", Context.MODE_PRIVATE);
        String json = preferences.getString("recipe","");
        Gson gson = new Gson();
        return gson.fromJson(json, RecipeSteps.class);
    }

}