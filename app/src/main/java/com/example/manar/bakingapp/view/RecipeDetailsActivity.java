package com.example.manar.bakingapp.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.manar.bakingapp.R;
import com.example.manar.bakingapp.SharedPrefrence;
import com.example.manar.bakingapp.model.RecipeSteps;

public class RecipeDetailsActivity extends AppCompatActivity {

    public static boolean isTwoPane = false;
    RecipeSteps recipeSteps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_details);
        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView title = toolbar.findViewById(R.id.title);
        setSupportActionBar(toolbar);
        // TODO in the activity to see if fragment 2 exists in the ui
        FrameLayout fragmentItemDetail = findViewById(R.id.fragment2);
        if (fragmentItemDetail != null) {
            isTwoPane = true;
        }
        Intent intent = getIntent();
//        if (intent.hasExtra("RecipeDetailsActivity")) {
//            recipeSteps = intent.getParcelableExtra("RecipeDetailsActivity");
//        }
        recipeSteps = SharedPrefrence.getData(getBaseContext());
        title.setText(recipeSteps.getName());

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        Bundle bundle = new Bundle();
        bundle.putParcelable("RecipeDetailsActivity", recipeSteps);
        RecipeDetailsFragment recipeDetailsFragment = new RecipeDetailsFragment();
        recipeDetailsFragment.setArguments(bundle);
//        getFragmentManager().beginTransaction().replace(R.id.fragment, recipeDetailsFragment).commit();
        if (isTwoPane)
            getFragmentManager().beginTransaction().replace(R.id.fragment, recipeDetailsFragment).addToBackStack(null).commit();
        else
            getFragmentManager().beginTransaction().replace(R.id.fragment, recipeDetailsFragment).addToBackStack(null).commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
//                int size = getFragmentManager().getFragments().size();
                if(getFragmentManager().getBackStackEntryCount()>1)
                    getFragmentManager().popBackStack();
                else
                    finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}