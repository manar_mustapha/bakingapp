package com.example.manar.bakingapp.view;


import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.manar.bakingapp.R;
import com.example.manar.bakingapp.adapter.IngredientsAdapter;
import com.example.manar.bakingapp.model.RecipeSteps;

public class IngredientListFragment extends Fragment {

    RecipeSteps recipeSteps;
    RecyclerView ingredientRecycleView;
    IngredientsAdapter ingredientsAdapter;
    public IngredientListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        recipeSteps = getArguments().getParcelable("ingredient");
        View view =  inflater.inflate(R.layout.fragment_ingredients, container, false);
        ingredientRecycleView = view.findViewById(R.id.ingredients);
        ingredientRecycleView.setHasFixedSize(true);
        ingredientRecycleView.setLayoutManager(new LinearLayoutManager(getActivity() , LinearLayoutManager.VERTICAL , false));
        ingredientsAdapter = new IngredientsAdapter(recipeSteps.getIngredients());
        ingredientRecycleView.setAdapter(ingredientsAdapter);
        return view;
    }

}