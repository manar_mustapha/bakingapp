package com.example.manar.bakingapp.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.manar.bakingapp.R;
import com.example.manar.bakingapp.adapter.DescriptionAdapter;
import com.example.manar.bakingapp.model.RecipeSteps;
import com.example.manar.bakingapp.model.Step;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A placeholder fragment containing a simple view.
 */
public class RecipeDetailsFragment extends Fragment {

    RecipeSteps recipeSteps;
    //    TextView ingredients;
//    RecyclerView descriptionRecycleView;
    DescriptionAdapter descriptionAdapter;

    @BindView(R.id.ingredients)
    TextView ingredients;
    @BindView(R.id.description)
    RecyclerView descriptionRecycleView;

    Unbinder unbinder;


    public RecipeDetailsFragment() {
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @OnClick({R.id.ingredients})
    void View(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ingredients: {
                Bundle bundle = new Bundle();
                bundle.putParcelable("ingredient", recipeSteps);
                IngredientListFragment ingredientListFragment = new IngredientListFragment();
                ingredientListFragment.setArguments(bundle);
                if (RecipeDetailsActivity.isTwoPane) {
                    getFragmentManager()
                            .beginTransaction()
                            .replace(R.id.fragment2, ingredientListFragment)
                            .addToBackStack(null)
                            .commit();
                } else {
                    getFragmentManager()
                            .beginTransaction()
                            .replace(R.id.fragment, ingredientListFragment)
                            .addToBackStack(null)
                            .commit();
                }
                break;
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recipe_details, container, false);
        recipeSteps = getArguments().getParcelable("RecipeDetailsActivity");
//        ingredients = view.findViewById(R.id.ingredients);
//        ingredients.setOnClickListener(this);
        unbinder = ButterKnife.bind(this, view);
//                ingredients.setOnClickListener(this);
//        descriptionRecycleView = view.findViewById(R.id.description);
        descriptionRecycleView.setHasFixedSize(true);
        descriptionRecycleView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        descriptionAdapter = new DescriptionAdapter(new DescriptionAdapter.OnClickHandler() {
            @Override
            public void onShortDescClicked(Step step) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("Step", step);
                StepDetailFragment stepDetailFragment = new StepDetailFragment();
                stepDetailFragment.setArguments(bundle);
                if (RecipeDetailsActivity.isTwoPane) {
                    getFragmentManager()
                            .beginTransaction()
                            .replace(R.id.fragment2, stepDetailFragment)
                            .addToBackStack(null)
                            .commit();
                } else {
                    getFragmentManager()
                            .beginTransaction()
                            .replace(R.id.fragment, stepDetailFragment)
                            .addToBackStack(null)
                            .commit();
                }
            }
        }, recipeSteps.getSteps(), getActivity());
        descriptionRecycleView.setAdapter(descriptionAdapter);
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (unbinder != null)
            unbinder.unbind();
    }
}