package com.example.manar.bakingapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.manar.bakingapp.R;
import com.example.manar.bakingapp.model.RecipeSteps;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by manar on 25/11/17.
 */

public class RecipeAdapter extends RecyclerView.Adapter<RecipeAdapter.RecipeViewHolder> {

    private RecipeAdapter.OnClickHandler onClickHandler;
    private List<RecipeSteps> recipeStepsList;
    private Context context;

    public RecipeAdapter(List<RecipeSteps> recipeStepsList, Context context, RecipeAdapter.OnClickHandler onClickHandler) {
        this.recipeStepsList = recipeStepsList;
        this.onClickHandler = onClickHandler;
        this.context = context;
    }

    public interface OnClickHandler {
        void onRecipeClicked(RecipeSteps recipeSteps);
    }

    @Override
    public RecipeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recipe_item, parent, false);
        return new RecipeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecipeViewHolder holder, int position) {
        holder.setImage(recipeStepsList.get(position).getImage());
        holder.setName(recipeStepsList.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return recipeStepsList.size();
    }

    class RecipeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        ImageView recipeImage;
        TextView recipeName;

        RecipeViewHolder(View itemView) {
            super(itemView);
            recipeImage = itemView.findViewById(R.id.recipe_image);
            recipeName = itemView.findViewById(R.id.recipe_name);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            onClickHandler.onRecipeClicked(recipeStepsList.get(position));
        }

        void setImage(String imgUrl){
            Glide.with(context).load(imgUrl).into(recipeImage);
        }
        void setName(String name){
            recipeName.setText(name);
        }
    }
}